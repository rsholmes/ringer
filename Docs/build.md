# Ringer build notes

## Information missing on silkscreen

The first run PCBs lacked some information on the silkscreen that would have been helpful. This has been corrected in the design files. The Molex connectors are labeled as follows:

![](../Images/molexes.png)

and the trimmers:

![](../Images/trimmerz.png)

## Calibration

To set the trimmers properly:

* Insert cables into inputs B and C, leave the other ends unconnected.
* Turn both attenuverters fully up.
* Verify the output is close to 0 V (there may be an offset of a few mV from the output op amp stage).
* Plug something into A, ideally a square wave from 0 V to 5 V, but a DC 5 V is okay.
* Adjust trimmer A until the output with 5 V is the same as with 0 V (close to 0 V).
* Move input from A to B.
* Adjust trimmer B until the output with 5 V is the same as with 0 V (close to 0 V).

