# ringer.kicad_sch BOM

Mon 08 Jan 2024 09:55:14 PM EST

Generated from schematic by Eeschema 7.0.10-7.0.10~ubuntu22.04.1

**Component Count:** 57


## 
| Refs | Qty | Component | Description | Vendor | SKU |
| ----- | --- | ---- | ----------- | ---- | ---- |
| C1, C2 | 2 | 10uF | Electrolytic capacitor, pitch 2.5 mm | Tayda | A-4349 |
| C3–8 | 6 | 100nF | Ceramic disc capacitor, pitch 2.5 mm | Tayda | A-553 |
| C9 | 1 | 10pF | Ceramic disc capacitor, pitch 2.5 mm | Tayda |  |
| D1, D2 | 2 | 1N5817 | Schottky Barrier Rectifier Diode, DO-41 | Tayda | A-159 |
| J1 | 1 | Synth_power_2x5 | Pin header 2.54 mm 2x5 | Tayda | A-2939 |
| J2, J3 | 2 | AudioJack2_SwitchT | Audio Jack, 2 Poles (Mono / TS), Switched T Pole (Normalling) | Tayda | A-1121 |
| J4, J12 | 2 | AudioJack2 | Audio Jack, 2 Poles (Mono / TS) | Tayda | A-1121 |
| J5, J6 | 2 | Molex Conn | KK254 Molex connector | Tayda | A-827 |
| J7, J8 | 2 | Molex Hdr | KK254 Molex header | Tayda | A-805 |
| J9, J10 | 2 | DIP-8 | 8 pin DIP socket |  |  |
| J11 | 1 | DIP-14 | 14 pin DIP socket | Tayda | A-004 |
| R1–3, R7, R9, R20, R21, R24 | 8 | 1k | Resistor | Tayda |  |
| R4, R6 | 2 | 47k | Resistor | Tayda |  |
| R5, R8 | 2 | 100k | Resistor | Tayda |  |
| R10–13 | 4 | 4.7k | Resistor | Tayda |  |
| R14–17, R22, R23 | 6 | 10k | Resistor | Tayda |  |
| R18, R19 | 2 | 300k | Resistor | Tayda |  |
| RV1, RV2 | 2 | B10k | Panel mount potentiometer | Tayda |  |
| RV3, RV4 | 2 | 50k | 3362P trimmer | Tayda |  |
| U1 | 1 | LM4040LP-5 | 5.000V Precision Micropower Shunt Voltage Reference, TO-92 |  |  |
| U2 | 1 | TL072 | Dual operational amplifier, DIP-8 | Tayda | A-037 |
| U3 | 1 | TL074 | Quad operational amplifier, DIP-14 | Tayda | A-1138 |
| U4 | 1 | AD633 | Analog multiplier |  |  |
| ZKN1, ZKN2 | 2 | Knob_MF-A03 | Knob |  |  |

### Resistors in color band order
|Value|Qty|Refs|
|----|----|----|
|1k|8|R1–3, R7, R9, R20, R21, R24|
|10k|6|R14–17, R22, R23|
|100k|2|R5, R8|
|300k|2|R18, R19|
|4.7k|4|R10–13|
|47k|2|R4, R6|

