# Ringer

This is a module based around the [AD633](https://www.analog.com/media/en/technical-documentation/data-sheets/ad633.pdf) (pdf) multiplier chip. It can serve as a 4-quadrant multiplier, VCA, or 2-channel attenuverting mixer.

The name "Ringer" is because:

* It is intended primarily as a 4-quadrant multiplier, also known (erroneously, in my view, but it's a common usage) as a *ring* modulator.
* As such it can be used to create *bell-like* sounds.
* It is inspired by Befaco's [A\*B+C](https://www.befaco.org/abc/), whose name reminds me of the mathematical concept of a *ring*.

I'm so clever I could throw up.

Functionally it's very similar to half an A\*B+C — half because the AD633 is crazy expensive! You might even say it's a dead *ringer* for half of an A\*B+C.
All right, all right, I'll stop. There are three inputs: `A`, `B`, and `C`. All are DC coupled and could be audio or CV. `B` and `C` have attenuverters on them. (The `B` attenuverter is labeled `AB` on the panel, since it only affects the product of `A` and `B`.) The output voltage is equal to `2×b×A×B/10+c×C`, where `b` and `c` are the attenuverter values ranging from -1 to 1. If nothing is plugged into `B` or `C` then they default to 5 V while `A` defaults to 0 V. Valid input voltage range on each input is about ±8 V. (Voltages up to ±12 V should not cause damage but above ±8 V will not give accurate results.)

So, for example:

* If `A` and `B` are ±5 V audio signals, the `AB` attenuverter is all the way up (`b` = 1), and the `C` attenuverter is centered (`c` = 0), the output is a ±5 V audio signal (`2×±5×1×±5/10+0 = ±5`) with frequency components that are sums and differences of the `A` and `B` frequency components — similar to a ring modulator.
* If `A` is a ±5 V audio signal, `B` is a 0 to 5 V envelope CV, the `AB` attenuverter is all the way up, and the `C` attenuverter is centered, the result is similar to a VCA with unity gain when `B` is 5 V. You can use the `AB` attenuverter to reduce the scale and the `C` attenuverter to add an offset. With the `AB` attenuverter all the way down it will sound the same as all the way up, but the phase of the output will be inverted.
* If inputs are plugged into `A` and `C`, then the module acts as a 2-channel attenuverting mixer.

The circuit design is different from Befaco's, however. Maybe it's just me but I found their attenuverter design odd and confusing. I prefer what Kassutronics calls a "[precision attenuverter](https://kassu2000.blogspot.com/2018/04/precision-attenuverter-mixer.html)", not only because it is simpler but also because it is insensitive to potentiometer tolerances and has an S-shaped response that makes it easier to set values near zero. (For a detailed look at two attenuverter designs see my blog post [Attenuators two](https://www.richholmes.xyz/analogoutput/2023-12-19_attenuverters-two/)). I also added trim pots to zero out offsets on both `A` and `B`. I chose to use a voltage reference instead of a resistive divider for the 5 V reference, and eliminated the indicator LED which didn't strike me as useful. The op amp thus saved was used to buffer the A input, not so much because a buffer is needed as because it serves to protect the expensive AD633 against overvoltage by letting the cheap op amp fry instead. (And the series 1k resistor helps guard against even that.)

## Current draw
24 mA +12 V, 16 mA -12 V


## Photos

![](Images/ringer.jpg)

## Documentation

* [Schematic](Docs/ringer_schematic.pdf)
* PCB layout: [front](Docs/Layout/ringer/ringer_F.SilkS,F.Mask.pdf), [back](Docs/Layout/ringer/ringer_B.SilkS,B.Mask.pdf)
* [BOM](Docs/ringer_bom.md)
* [Build notes](Docs/build.md)
* [Blog post](https://www.richholmes.xyz/analogoutput/2024-01-08_ringer/)
* [Video](https://diode.zone/w/tqo7v2pCiqN2RwK6AWg6YT)

## Git repository

* [https://gitlab.com/rsholmes/ringer](https://gitlab.com/rsholmes/ringer)


